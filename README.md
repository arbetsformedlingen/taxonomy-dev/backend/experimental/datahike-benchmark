# Datahike Benchmark

This tool is used to evaluate and compare query times for different versions of Datahike and Datomic. It is implemented using Babashka and Clojure.

## Quick start

To run the current configuration for all targets and see the results,

1. Make sure you have Babashka and Clojure installed.
2. Clone this repo and checkout the branch of choice.
3. Run the following:
   ```
   ./benchmark.clj run-all-targets
   ./benchmark.clj summary-report
   ```

## Configuration

The targets to compare are specified at the `:targets` key in the file [`config.edn`](config.edn). The `:ref-target` key can optionally be used to specified a target against which relative times are computed. If this target is not specified, the first target at the `:targets` key is used instead.

## Evaluating query times

To evaluate the query times of all targets, call

```
./benchmark.clj run-all-targets
```

To evaluate the query times of a specific target, for example the target with key `:my-target`, call

```
./benchmark.clj run-target my-target
```

The results will be stored in the the `results` subdirectory in individual directories per target.

## Report

To get a summary report with total query times per target, call

```
./benchmark.clj summary-report
```

which will produce something like

```
TARGET                  ABS TIME (s)   REL TIME
Jonas Github Datahike         84.548       103%
Official Datahike             81.694       100%
Experimental Datahike         58.278        71%
```

To get a report with times for individual queries included, call

```
./benchmark.clj full-report
```

## Profiling

To produce a clj-async-profiler flame graph, call `exercise-target` and specify number of iterations with `--iters` and also provide the `--profile` flag:
```
./benchmark.clj exercise-target datahike-0.6.1588 --iters 3000 --profile
```
The reports will be found under `/tmp/clj-async-profiler`.

You can also run `exercise-target` without additional options. This is useful when using the sampler in VisualVM:

```
./benchmark.clj exercise-target datahike-0.6.1588
```
Once the script is running, open VisualVM and start recording samples. Then stop recording samples and inspect the results.

## License

Copyright 2024 Jonas Östlund

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



