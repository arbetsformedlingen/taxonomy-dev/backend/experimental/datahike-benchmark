#!/usr/bin/env bb
(ns benchmark
  (:require [clojure.edn :as edn]
            [babashka.fs :as fs]
            [clojure.string :as str]
            [clojure.spec.alpha :as spec]
            [babashka.process :refer [shell]]
            [clojure.pprint :as pp]
            [clojure.walk :refer [postwalk]]
            [babashka.cli :as cli]
            [clojure.pprint :as pp])
  (:import [java.util UUID]))

(spec/def :mvn/version string?)
(spec/def :git/url string?)
(spec/def :git/sha string?)
(spec/def :local/root string?)
(spec/def ::dep (spec/or :maven (spec/keys :req [:mvn/version])
                         :git (spec/keys :req [:git/url :git/sha])
                         :local (spec/keys :req [:local/root])))
(spec/def ::deps (spec/map-of symbol? ::dep))
(spec/def ::type #{:datomic :datahike})
(spec/def ::key keyword?)
(spec/def ::target (spec/keys :req-un [::type ::deps ::key]))
(spec/def ::targets (spec/coll-of ::target))
(spec/def ::database-file string?)
(spec/def ::config (spec/keys :req-un [::targets ::database-file]))

(defn with-slice? [target-config]
  (:with-slice? target-config true))

(defn prepare-result-for-comparison [result]
  (let [ints-removed (postwalk (fn [x] (if (int? x) ::int x)) result)
        seqs-to-set (postwalk (fn [x] (if (and (sequential? x)
                                               (not (map-entry? x))
                                               (not= ints-removed x))
                                        (set x)
                                        x))
                              ints-removed)]
    (set seqs-to-set)))

(defn check-target-for-comparison [x]
  (let [info {:key (:key x)}]
    (when (with-slice? x)
      (throw (ex-info "Cannot compare results of target because slice" info)))
    (when-not (:save-results? x)
      (throw (ex-info "No results saved for target")))))

(defn compare-results [a b]
  (check-target-for-comparison a)
  (check-target-for-comparison b)
  (into []
        (remove nil?)
        (map (fn [i ax bx]
               (println "Compare" i)
               (when (not= (-> ax :result prepare-result-for-comparison)
                           (-> bx :result prepare-result-for-comparison))
                 (println "   Not equal")
                 {:index i
                  :query (:query ax)
                  :results {(:key a) (:result ax)
                            (:key b) (:result bx)}}))
             (range)
             (:results a)
             (:results b))))

(defn load-edn [f]
  (-> f
      fs/file
      slurp
      edn/read-string))

(defn load-config []
  (let [config-data (load-edn "config.edn")
        config (spec/conform ::config config-data)]
    (when (= ::spec/invalid config)
      (throw (ex-info (str "Invalid config: "
                           (spec/explain-str ::config config-data))
                      {:config-data config-data})))
    (let [target-keys (->> config
                           :targets
                           (map :key)
                           frequencies
                           (keep (fn [[k n]] (when (< 1 n) k))))]
      
      (when (seq target-keys)
        (throw (ex-info "Non-unique target keys" {:keys target-keys}))))
    (let [targets (:targets config)
          config (merge {:ref-target (-> targets first :key)} config)
          target-key-set (into #{} (map :key) targets)]
      (when-not (contains? target-key-set (:ref-target config))
        (throw (ex-info "Invalid ref target" config)))
      config)))

(defn target-map [config]
  (into {}
        (map (fn [target] [(:key target) target]))
        (:targets config)))

(defn get-datahike-jars [repo-root]
  (let [target-path (fs/path repo-root "target")]
    (if (fs/exists? target-path)
      (filter
       #(str/ends-with? (str %) ".jar")
       (fs/list-dir target-path))
      [])))

(defn build-jar-at [root]
  (doseq [f (get-datahike-jars root)]
    (fs/delete f))
  (when-not (= 0 (:exit (shell {:dir root :out :string} "bb" "jar")))
    (throw (ex-info "Failed to call `bb jar`"
                    {:local-root root})))
  (str (first (get-datahike-jars root))))


(defn wrap-local-root [jar-file]
  {:local/root (str jar-file)})

(defn wrap-local-root-at-datahike [datahike-repo-root]
  (let [jars (get-datahike-jars datahike-repo-root)]
    (assert (= 1 (count jars)))
    (wrap-local-root (first jars))))

(defn get-cache-key [cache-key-file full-key]
  (let [old-cache (if (fs/exists? cache-key-file)
                    (load-edn cache-key-file)
                    {})
        new-cache (update old-cache
                          full-key
                          #(or % (str (UUID/randomUUID))))]
    (spit cache-key-file (pr-str new-cache))
    (get new-cache full-key)))

(defn datahike-dep-from-local [repo-path]
  (build-jar-at repo-path)
  (wrap-local-root-at-datahike repo-path))

(defn datahike-dep-from-git [git-url git-sha]
  (let [repo-key (get-cache-key "repocache.edn" [git-url git-sha])
        cache-path (fs/path "repocache" repo-key)]
    (when (empty? (get-datahike-jars cache-path))
      (fs/create-dirs "repocache")
      (when-not (fs/exists? cache-path)
        (shell {:dir "repocache"}
               "git clone"
               git-url
               repo-key))
      (shell {:dir cache-path} "git checkout" git-sha)
      (build-jar-at cache-path))
    (wrap-local-root-at-datahike cache-path)))

(defn make-datahike-deps [deps]
  (into {}
        (map (fn [[k [dep-type dep-data]]]
               [k (case dep-type
                    :maven dep-data
                    :git (datahike-dep-from-git (:git/url dep-data) (:git/sha dep-data))
                    :local (datahike-dep-from-local (:local/root dep-data)))]))
        deps))

(defn make-datomic-deps [deps]
  (into {}
        (map (fn [[k [_dep-type dep-data]]]
               [k dep-data]))
        deps))

(def base-paths ["src" "resources"])

(defn write-context [filename target]
  (spit (fs/file filename)
        (with-out-str (pp/pprint target))))

(defn load-base-deps []
  (edn/read-string (slurp "base_deps.edn")))

(defn deps-edn-for-target


  #_[targets target-key]
  #_{:pre [(map? targets)
         (contains? targets target-key)]}

  [{:keys [type deps] :as target}]
  {:pre [(keyword? type)
         (map? deps)
         (map? target)]}
  
  (let [ ;;target (targets target-key)
        {:keys [type deps]} target]
    (write-context "context.edn" target)
    (let [extra-deps
          (case type
            :datahike {:deps (make-datahike-deps deps)
                       :paths (conj base-paths "env/datahike")}
            :datomic {:deps (merge
                             { ;; Needed for normalize-q-input
                              'io.replikativ/datahike {:mvn/version "0.6.1558"}

                              ;; Needed for populating the database
                              'io.lambdaforge/wanderung
                              {:git/url "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/wanderung.git"
                               :git/sha "c8edc5012f47fab1b2e1b765732e338005aaf641"}}
                             (make-datomic-deps deps))
                      :paths (conj base-paths "env/datomic")})]
      (merge-with merge (load-base-deps) extra-deps))))

(defn download-db-if-needed [db-name]
  (when-not (fs/exists? db-name)
    (shell
     "wget"
     (format
      "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist/-/raw/main/resources/%s"
      db-name))))

(defn set-target [config target-key]
  (download-db-if-needed (:database-file config))
  (spit "deps.edn"
        (with-out-str
          (pp/pprint
           (deps-edn-for-target
            (get (target-map config) target-key))))))

(defn generate-split []
  (let [query-count (-> "resources/db_queries.edn"
                        load-edn
                        :data
                        count)
        inds (-> query-count range shuffle vec)
        middle (quot query-count 2)]
    (spit "resources/split.edn"
          {:a (subvec inds 0 middle)
           :b (subvec inds middle)})))

(def result-root "results")

(defn load-result-files [root]
  (for [f (fs/list-dir root)
        :when (str/starts-with? (str (fs/file-name f)) "split_")]
    [f (load-edn f)]))



(defn accumulate-results-from [dst root]
  (reduce (fn [dst [f data]]
            (update dst f into data))
          dst
          (load-result-files root)))

(defn result-query-index [result-item]
  (-> result-item
      :query
      :query-index))

(defn average-acc-data [v]
  (->> v
       (group-by result-query-index)
       (sort-by first)
       vals
       (into [] (map (fn [group]
                       (let [n (count group)]
                         (case n
                           1 (first group)
                           (assoc (first group)
                                  :elapsed-ns
                                  (double
                                   (/ (transduce (map :elapsed-ns)
                                                 + 0 group)
                                      n))))))))))

(defn average-accumulated-results [results]
  (into {}
        (map (fn [[k v]]
               [k (average-acc-data v)]))
        results))



(defn evaluate-target [config target-key]
  (let [result-path (fs/path result-root (name target-key))
        targets (target-map config)
        target (get targets target-key)
        iters (:iters config 1)
        _ (set-target config target-key)
        _ (fs/create-dirs result-path)
        acc (reduce (fn [dst i]
                      (println (format "RUN %s ITERATION %d/%d" (name target-key) (inc i) iters))
                      (println "Run with warmup :a and measured :b")
                      (shell "clj -X:task run-cli :warmup-split-key :a :measured-split-key :b")
                      (println "Run with warmup :b and measured :a")
                      (shell "clj -X:task run-cli :warmup-split-key :b :measured-split-key :a")
                      (accumulate-results-from dst result-path))
                    {}
                    (range iters))]
    (doseq [[f data] (average-accumulated-results acc)]
      (println "Write" (str f))
      (spit (fs/file f) (pr-str data)))
    (write-context (fs/path result-path "context.edn") target)))

(defn exercise-target [input]
  (let [config (load-config)
        opts (:opts input)
        target-key (-> opts :target keyword)
        iters (:iters opts)
        profile? (str (:profile opts))]
    (println "target-key" target-key)
    (set-target config target-key)
    (shell (format "clj -X:task exercise :limit %d :profile? %s" iters (str profile?)))))

(defn provide-cli-input-to-config [config input]
  (merge config
         (:opts input)))

(defn run-target [input]
  (let [target-key (-> input :opts :target keyword)
        config (provide-cli-input-to-config (load-config) input)]
    (evaluate-target config target-key)))

(defn run-all-targets [input]
  (let [config (provide-cli-input-to-config (load-config) input)]
    (doseq [target (:targets config)]
      (println "Run target" target)
      (evaluate-target config (:key target)))))

(defn load-results [root]
  (->> (for [[_f data] (load-result-files root)
             item data]
         item)
       (sort-by result-query-index)
       vec))

(defn load-all-results
  ([config] (load-all-results config (constantly true)))
  ([config pred]
   (let [target-order (into {}
                            (map-indexed
                             (fn [i target]
                               [(:key target) i]))
                            (:targets config))
         order-fn (fn [context] (get target-order
                                     (:key context)
                                     Integer/MAX_VALUE))]
     (vec (sort-by order-fn
                   (for [p (fs/list-dir result-root)
                         :let [context-file (fs/path p "context.edn")]
                         :when (fs/exists? context-file)
                         :let [context (load-edn context-file)]
                         :when (pred (:key context))]
                     (assoc context :results (load-results p))))))))


(defn provide-width [x]
  (merge {:width (count (:value x))} x))

(defn normalize-cell [cell]
  (provide-width
   (cond
     (string? cell) {:value cell :adjust :left}
     (nil? cell) {:value "" :adjust :left}
     (not (map? cell)) (throw (ex-info "Invalid cell" {:cell cell}))
     (not (string? (:value cell))) (throw (ex-info "Missing cell value" {:cell cell}))
     :else (merge {:adjust :left} cell))))

(defn cells-max-width [& cells]
  (apply max (map :width cells)))

(defn table-precomp [rows]
  (let [rows (mapv #(mapv normalize-cell %) rows)
        col-count (count (first rows))
        col-widths (apply map cells-max-width rows)]
    (doseq [row rows]
      (assert (= col-count (count row))))
    {:col-widths col-widths
     :col-count col-count
     :rows rows}))

(defn render-cell [{:keys [width adjust value]} col-width]
  (let [pad (apply str (repeat (max 0 (- col-width width)) " "))]
    (case adjust
      :left (str value pad)
      :right (str pad value))))

(defn render-table
  ([rows] (render-table rows {:sep "   "}))
  ([rows {:keys [sep]}]
   {:pre [(sequential? rows)
          (every? sequential? rows)]}
   (let [{:keys [rows col-widths]}
         (table-precomp rows)]
     (str/join "\n"
               (for [row rows]
                 (str/join
                  sep
                  (map (fn [width cell]
                         (render-cell cell width))
                       col-widths
                       row)))))))

(defn demo-render-table []
  (render-table [["Mass" "Height" "BMI"]
                 ["73" "1.73" {:value "24" :adjust :right}]]))

(defn compute-total-time [context]
  (let [total-time-ns (apply + (map :elapsed-ns (:results context)))]
    (assoc context :total-time-ns total-time-ns)))

(defn render-percents [f]
  (format "%d%%" (int (Math/round (* 100.0 f)))))

(defn format-time-ns [x]
  (format "%.3f" (* 1.0e-9 x)))

(defn summary-report [_]
  (let [config (load-config)
        results (map compute-total-time (load-all-results config))
        ref-time-ns (first (for [{:keys [total-time-ns key]} results
                                 :when (= key (:ref-target config))]
                             total-time-ns))]
    (println
     (render-table
      (into [["TARGET" "ABS TIME (s)" "REL TIME" "SLICE?"]]
            (map (fn [{:keys [label total-time-ns] :as result}]
                   [label
                    {:adjust :right
                     :value (format-time-ns total-time-ns)}
                    {:adjust :right
                     :value (render-percents (/ total-time-ns ref-time-ns))}
                    (str (str (with-slice? result)))]))
            results)))))

(defn result-index-set [results]
  (into #{} (map result-query-index) results))

(defn abbrev [s n]
  (let [m (count s)]
    (if (<= m n)
      s
      (str (subs s 0 (- n 3)) "..."))))

(defn full-report [& _]
  (let [config (load-config)
        results (map compute-total-time (load-all-results config))
        index-sets-groups (group-by (comp result-index-set :results) results)
        _ (when (< 1 (count index-sets-groups))
            (throw (ex-info "Inconsistent index sets"
                            {:groups index-sets-groups})))
        inds (-> index-sets-groups keys first sort)
        headers (into ["QUERY"] (map (fn [x] {:adjust :right :value (:label x)})) results)
        data-rows (for [index inds]
                    (into [(str index)]
                          (map (fn [{:keys [results]}]
                                 (format-time-ns (:elapsed-ns (nth results index)))))
                          results))
        total-row (into ["TOTAL TIME"]
                        (map (fn [{:keys [total-time-ns]}]
                               (format-time-ns total-time-ns)))
                        results)]
    (println (render-table (into [] cat [[headers] data-rows [total-row]])))))

(defn list-targets [& _]
  (doseq [target-key (->> (load-config) :targets (map :key))]
    (println (name target-key))))

(defn list-queries [& _]
  (doseq [[i query-data] (map-indexed vector (:data (load-edn "resources/db_queries.edn")))]
    (println (format "--- Query %d" i))
    (println (abbrev (with-out-str (pp/pprint query-data)) 1000))
    (println "\n")))

(defn disp-query [input]
  (let [query-index (-> input :opts :query Long/valueOf)
        queries (vec (:data (load-edn "resources/db_queries.edn")))
        query (nth queries query-index)]
    (pp/pprint query)))

(defn cli-compare-results [input]
  (let [{:keys [target-a target-b out-file]} (:opts input)
        target-a (keyword target-a)
        target-b (keyword target-b)
        targets-of-interest #{target-a target-b}
        config (load-config)
        results (load-all-results
                 config
                 #(contains? targets-of-interest %))
        _ (when (not= 2 (count results))
            (throw (ex-info "Did not find all results" {:keys (map :key results)})))
        [a b] results
        comparison (compare-results a b)]
    (println "The results differ for" (count comparison) "queries")
    (spit out-file (with-out-str
                     (pp/pprint comparison)))))

(def table
  [{:cmds ["summary-report"] :fn summary-report}
   {:cmds ["list-queries"] :fn list-queries}
   {:cmds ["disp-query"] :fn disp-query :args->opts [:query]}
   {:cmds ["full-report"] :fn full-report}
   {:cmds ["list-targets"] :fn list-targets}
   {:cmds ["run-target"] :fn run-target :args->opts [:target]}
   {:cmds ["run-all-targets"] :fn run-all-targets}
   {:cmds ["exercise-target"] :fn exercise-target :args->opts [:target]}
   {:cmds ["compare-results"] :fn cli-compare-results :args->opts [:target-a :target-b :out-file]}])

(defn -main [& args]
  (when (seq args)
    (cli/dispatch table args {:coerce {:iters :long
                                       :profile :boolean}})))

(apply -main *command-line-args*)

(defn demo []
  (let [config (load-config)
        targets (target-map config)]
    (deps-edn-for-target (:datomic targets))))

(comment

  (set-target (load-config) :datahike-refactored-wip)

  (evaluate-target (load-config) :datahike-0.6.1558)

  (def results (load-results "results/wip"))
  
  (def results (-> {}
                   (accumulate-results-from "results/wip")
                   (accumulate-results-from "results/wip")
                   (accumulate-results-from "results/wip")
                   (accumulate-results-from "results/wip")))


  (def avg-results (average-accumulated-results results))
  
  (def x (-> results vals first first))

  )
