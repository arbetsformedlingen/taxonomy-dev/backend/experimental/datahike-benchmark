(ns datahike-benchmark.core
  (:require [datahike-benchmark.backend :as backend]
            [datahike.query :as dq]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [taoensso.nippy :as nippy]
            [clojure.walk :refer [postwalk]]
            [babashka.fs :as fs]
            [clojure.string :as str]
            [clj-async-profiler.core :as prof]))

#_(defn download-database-file [src-db-filename]
  (let [src-url (format "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist/-/raw/main/resources/%s"
           src-db-filename)
        req (http-client/get src-url {:as :byte-array :throw-exceptions false})]
     (when (= 200 (:status req))
       (:body req))))

(defn remove-query-slicing [query]
  {:post [(let [s (str %)]
            (and (not (str/includes? s "offset"))
                 (not (str/includes? s "limit"))))]}
  (postwalk (fn [x]
              (cond
                (map? x) (dissoc x :offset :limit)
                :else x))
            query))

(defn read-edn [f]
  (-> f
      slurp
      edn/read-string))

(defn load-queries []
  (-> "db_queries.edn"
      io/resource
      read-edn))

(defn load-split []
  (-> "split.edn"
      io/resource
      read-edn))

(defn load-config []
  (read-edn "config.edn"))

(defn load-context []
  (read-edn "context.edn"))

(defn evaluate-query-db-values [query conn]
  (let [db (backend/db conn)]
    (postwalk
     (fn [x]
       (if (and (map? x)
                (contains? x :taxonomy-db/type))
         (if-let [version (:taxonomy-db/version-id x)]
           (let [tx (backend/q
                     {:query
                      '[:find ?tx
                        :in $ ?version
                        :where
                        [?t :taxonomy-version/id ?version]
                        [?t :taxonomy-version/tx ?tx]],
                      :args [db version]})]
             (when-not (= 1 (count tx))
               (throw (ex-info "Unexpected tx count"
                               {:version version
                                :tx tx})))
             (backend/as-of db (ffirst tx)))
           db)
         x))
     query)))

(defn decorate-query [{:keys [query args]} extra]
  {:query (merge (dq/normalize-q-input query args)
                 extra)
   :args []})

(defn measure-query-time [raw-query conn extra-query-data save-results?]
  (let [{:keys [query args] :as _exact-query} (decorate-query
                                               (evaluate-query-db-values raw-query conn)
                                               extra-query-data)
        start (System/nanoTime)
        result (doall (apply backend/q query args))
        end (System/nanoTime)]
    (merge {:elapsed-ns (- end start)
            :result-count (count result)
            :query raw-query}
           (if save-results?
             {:result result}
             {}))))

(defn select-queries [queries inds]
  (mapv #(assoc (nth queries %)
                :query-index %)
        inds))

(defn run-benchmark-for-queries
  [db queries extra-query-data save-results?]
  (into []
        (map (fn [query]
               (measure-query-time query db extra-query-data
                                   save-results?)))
        queries))

(defn run-full-benchmark [db
                          warmup-queries
                          measured-queries
                          extra-query-data
                          save-results?]
  (run-benchmark-for-queries db
                             warmup-queries
                             extra-query-data
                             save-results?)
  (run-benchmark-for-queries db
                             measured-queries
                             extra-query-data
                             save-results?))

(defn initialize-db [config]
  (-> config
      :database-file
      nippy/thaw-from-file
      backend/database-from-datoms))

;; clj -X:task run-cli :warmup-split-key :a :measured-split-key :b
(defn run-cli [{:keys [warmup-split-key measured-split-key]}]
  (let [config (load-config)
        context (load-context)
        db (initialize-db config)
        with-slice? (:with-slice? context true)
        save-results? (:save-results? context false)
        raw-queries (:data (load-queries))
        queries (cond->> raw-queries
                  (not with-slice?) (map remove-query-slicing))
        split (load-split)
        warmup-queries (select-queries
                        queries
                        (get split warmup-split-key))
        measured-queries (select-queries
                          queries
                          (get split measured-split-key))
        results (run-full-benchmark
                 db
                 warmup-queries
                 measured-queries
                 (:extra-query-data context)
                 save-results?)
        result-root (fs/path "results" (name (:key context)))
        filename (str "split_" (name measured-split-key) ".edn")]
    (fs/create-dirs result-root)
    (spit (fs/file result-root filename)
          (pr-str results))))

(defn run-random-query [queries db extra-query-data]
  {:pre [(sequential? queries)]}
  (let [n (count queries)
        i (rand-int n)]
    (assoc (measure-query-time (nth queries i) db extra-query-data false)
           :query-index i)))

(def period-ms 3000)

(defn exercise [{:keys [limit profile?]}]
  (println "\n\n------ limit" limit)
  (println "------ profile?" profile?)
  (let [config (load-config)
        db (initialize-db config)
        queries (:data (load-queries))
        context (load-context)
        extra-query-data (:extra-query-data context)
        body (fn []
               (loop [counter 0
                      next-disp 0]
                 (if (and limit (<= limit counter))
                   (println "Finished after" counter "iterations")
                   (do (run-random-query queries
                                         db extra-query-data)
                       (recur (inc counter)
                              (let [c (System/currentTimeMillis)]
                                (if (< next-disp c)
                                  (do (println "Completed"
                                               (inc counter)
                                               "iterations")
                                      (+ c period-ms))
                                  next-disp)))))))]
    (println "Exercise" (:label context) (if limit (format "for %d iterations" limit) ""))
    (if profile?
      (prof/profile (body))
      (body))))

(defn demo []
  (run-cli {:warmup-split-key :a :measured-split-key :b}))


(comment

  (def context (load-context))
  (def split (load-split))
  (def queries (load-queries))

  (def query (-> queries :data second))
  
  (def config (load-config))
  (def datoms (nippy/thaw-from-file (:database-file config)))
  (def db (backend/database-from-datoms datoms))

  (def equery (evaluate-query-db-values query db))
  (def decorated-query (decorate-query equery {:mjao 9}))

  (def result (measure-query-time query db {}))

  (def warmup-queries (select-queries (:data queries)
                                      (:a split)))

  (def measured-queries (select-queries (:data queries)
                                        (:b split)))

  (def result (run-full-benchmark db
                                  warmup-queries
                                  measured-queries
                                  {}))

  (run-cli {:warmup-split-key :a :measured-split-key :b})

  

  )

;; open /tmp/clj-async-profiler/results/01-cpu-flamegraph-2024-10-08-17-21-19.html
