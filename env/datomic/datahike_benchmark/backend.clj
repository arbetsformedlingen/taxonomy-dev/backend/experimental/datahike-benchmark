(ns datahike-benchmark.backend
  (:require [datomic.client.api :as datomic]
            [wanderung.datomic-cloud :as wdc])
  (:import [java.util UUID]))

(defn base-config [suffix]
  {:db-name (format "db-name-%s" suffix)
   :server-type :datomic-local
   :system "testing"
   :storage-dir :mem})

(defn database-from-datoms [datoms]
  (let [cfg (base-config (str (UUID/randomUUID)))
        client (datomic/client cfg)
        _ (datomic/create-database client cfg)
        conn (datomic/connect client cfg)]
    (wdc/transact-datoms-to-datomic
     conn
     datoms
     {:max-transaction-size 100000})
    conn))

;(def connect datomic/connect)
(def q datomic/q)
(def db datomic/db)
(def as-of datomic/as-of)


;;(wdc/transact-datoms-to-datomic)

