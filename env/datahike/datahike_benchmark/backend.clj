(ns datahike-benchmark.backend
  (:require [datahike.api :as d]))

(def db-config {:keep-history? true, :keep-history true, :search-cache-size 10000, :index :datahike.index/persistent-set, :store {:id "in-mem-nippy-data2", :backend :mem}, :name "Nippy testing data", :store-cache-size 1000, :wanderung/type :datahike, :attribute-refs? true, :writer {:backend :self}, :crypto-hash? false, :schema-flexibility :write, :branch :db})

(def q d/q)
(def as-of d/as-of)
(def db d/db)

(defn database-from-datoms [datoms]
  (let [db (d/create-database db-config)
        conn (d/connect db)
        _ (deref (d/load-entities conn datoms))]
    conn))
